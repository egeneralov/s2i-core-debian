# This image is the base image for all s2i configurable container images.
FROM debian:9

ENV SUMMARY="Base image which allows using of source-to-image."	\
    DESCRIPTION="The s2i-core image provides any images layered on top of it \
with all the tools needed to use source-to-image functionality while keeping \
the image size as small as possible." \
    STI_SCRIPTS_URL=image:///usr/libexec/s2i \
    STI_SCRIPTS_PATH=/usr/libexec/s2i \
    APP_ROOT=/opt/app-root \
    HOME=/opt/app-root/src \
    PATH=/opt/app-root/src/bin:/opt/app-root/bin:$PATH


LABEL summary="$SUMMARY" \
      description="$DESCRIPTION" \
      io.k8s.description="$DESCRIPTION" \
      io.k8s.display-name="s2i core" \
      io.openshift.s2i.scripts-url=image:///usr/libexec/s2i \
      io.s2i.scripts-url=image:///usr/libexec/s2i \
      com.redhat.component="s2i-core-container" \
      name="egeneralov/s2i-core-debian" \
      version="1" \
      release="1" \
      maintainer="Eduard Generalov <eduard@generalov.net>"

RUN INSTALL_PKGS="bsdtar findutils gettext groff-base tar unzip apt-utils python" && \
  apt-get update -q && \
  apt-get install -yq ${INSTALL_PKGS} && \
  apt-get autoremove -yq && \
  apt-get autoclean -yq && \
  apt-get clean -yq

COPY ./root/ /

WORKDIR ${HOME}

CMD ["base-usage"]

RUN useradd -u 1001 -r -g 0 -d ${HOME} -s /sbin/nologin -c "Default Application User" default && chown -R 1001:0 ${APP_ROOT}
