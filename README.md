OpenShift core image for debian-based solutions
======================================================

This repository contains Dockerfiles for images which can be used as base images
to add support for [source-to-image](https://github.com/openshift/source-to-image)
without installing several development libraries.


WARNING
=======

This a fork.

- based on debian
- more minimal
- more clean
